package fr.william.technical_test_square;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Liste des contacts");

        JSONArray jsonArray = getJsonArrayFromAsset("technical_test.json");

        if (jsonArray != null) {
            userList = getListFromJsonArray(jsonArray);

            MyCustomAdapter adapter = new MyCustomAdapter(this, userList);
            ListView listView = findViewById(R.id.listview);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);
        } else Toast.makeText(getApplicationContext(), "Impossible de charger les contacts !", Toast.LENGTH_LONG).show();
    }

    //Récupère le fichier JSON depuis le dossier "assets" et retourne un JSONArray
    private JSONArray getJsonArrayFromAsset(String filename) {
        try {
            InputStream inputStream = getApplicationContext().getAssets().open(filename);
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) responseStrBuilder.append(inputStr);

            return new JSONObject(responseStrBuilder.toString()).getJSONArray("items");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    //Ajoute les utilisateurs du JSON à une liste et retourne la liste
    private List<User> getListFromJsonArray(JSONArray jsonArray) {
        List<User> userList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonUser = jsonArray.getJSONObject(i);
                userList.add(new User(jsonUser.getString("name"), jsonUser.getString("surname"), jsonUser.getString("fullname"), jsonUser.getString("email"), jsonUser.getString("photo"), jsonUser.getInt("age")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Trie la liste par nom
        Collections.sort(userList, (u1, u2) -> u1.fullname.compareTo(u2.fullname));

        return userList;
    }

    //Créé un email et demande à l'utilisateur l'application à utiliser
    private void sendEmail(String email, String msg) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Envoyer un mail :"));
    }

    //Lors du clic sur un élément de la liste
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        sendEmail(userList.get(i).email, "Bonjour " + userList.get(i).fullname.split(" ")[0] + ",\n");
    }
}