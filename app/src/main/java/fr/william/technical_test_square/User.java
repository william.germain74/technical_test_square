package fr.william.technical_test_square;

public class User {
    String name, surname, fullname, email, imageUrl;
    int age;

    public User(String name, String surname, String fullname, String email, String imageUrl, int age) {
        this.name = name;
        this.surname = surname;
        this.fullname = fullname;
        this.email = email;
        this.imageUrl = imageUrl;
        this.age = age;
    }
}