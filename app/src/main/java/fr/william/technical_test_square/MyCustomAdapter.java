package fr.william.technical_test_square;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyCustomAdapter extends BaseAdapter {

    private final List<User> userList;
    private final LayoutInflater layoutInflater;

    public MyCustomAdapter(Context context, List<User> userList) {
        this.userList = userList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item, null);
            holder = new ViewHolder();
            holder.photo = view.findViewById(R.id.photo);
            holder.fullname = view.findViewById(R.id.fullname);
            holder.surname = view.findViewById(R.id.surname);
            holder.email = view.findViewById(R.id.email);
            holder.age = view.findViewById(R.id.age);
            view.setTag(holder);
        } else holder = (ViewHolder) view.getTag();

        holder.fullname.setText(userList.get(position).fullname);
        holder.surname.setText("Surnom : " + userList.get(position).surname);
        holder.email.setText("Email : " + userList.get(position).email);
        holder.age.setText(" - " + userList.get(position).age + " ans");

        Picasso.get().load(userList.get(position).imageUrl).into(holder.photo);

        return view;
    }

    static class ViewHolder {
        ImageView photo;
        TextView fullname, email, age, surname;
    }
}